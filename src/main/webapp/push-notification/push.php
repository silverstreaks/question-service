

function send_push_message($subscription_ids)
  // Set GCM endpoint
  $url = 'https://android.googleapis.com/gcm/send';

  $fields = array(
      'registration_ids' => $subscription_ids,
  );

  $headers = array(
      'Authorization: key=' . AIzaSyCMRNeS6dX2WDJTANfkn5XxS6ll3iKhxKY,
      'Content-Type: application/json'
  );

  $ch = curl_init();

  // Set the url, number of POST vars, POST data
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

  // Execute post
  $result = curl_exec($ch);
  if ($result === FALSE) {
      die('Push msg send failed in curl: ' . curl_error($ch));
  }

  // Close connection
  curl_close($ch);
}