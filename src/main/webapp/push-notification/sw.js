
self.addEventListener('push', function(e) {
  console.log('push received');

    var httpHeaders = new Headers();
    httpHeaders.append('pragma', 'no-cache');
    httpHeaders.append('cache-control', 'no-cache');

    var fetchInit = {
      method: 'GET',
      headers: httpHeaders,
    };

    fetch("https://mobiforge.com/push/latest.json", fetchInit).then(function(res) {
      res.json().then(function(data) {
       if(Notification.permission=='granted') {
               // Get service worker to show notification
                self.registration.showNotification(notificationData.data.title, {
                   body: notificationData.data.body,
                   icon: 'favicon.ico'
               });
           }
           else {
             //We need to ask permission
             Notification.requestPermission(function(permission) {
               if(permission=='granted') {
                 self.registration.showNotification(notificationData.data.title, {
                   body: notificationData.data.body,
                   icon: 'favicon.ico'
                 });
               }
             });
          }
    })
  })
});