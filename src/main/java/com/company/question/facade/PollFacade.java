package com.company.question.facade;

import com.company.question.model.*;
import com.company.question.server.Server;
import com.company.question.server.UserProfileServer;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.company.question.util.Constants.*;

/**
 * Created by Chandan on 22-02-2017 22:28.
 */
public class PollFacade extends Server {
    private final static Logger log = Logger.getLogger(PollFacade.class);

    public boolean addQuestion(Poll poll) throws Exception {
        if (poll != null) {
            dataClient.setCollection(POLL_COLL);
            return dataClient.saveData(createPoll(poll));
        }
        return false;
    }

    public List<Poll> getQuestionsById() throws Exception {
        return null;
    }




    public List<Poll> getQuestions() throws Exception {
        dataClient.setCollection(POLL_COLL);
        Iterator<DBObject> dbObjectIterator = dataClient.readData();
        List<Poll>  pollList = new ArrayList<>();

        while (dbObjectIterator.hasNext()) {

            BasicDBObject object = (BasicDBObject) dbObjectIterator.next();
            Poll poll = new Poll();
            poll.setId(object.get("_id").toString());
            poll.setQuestion((String) object.get("question"));
            poll.setShowIdentity((String) object.get("showIdentity"));
            poll.setUserProfile(new UserProfile((String) object.get("user-id")));

            List<Tag> tagList = null;
            BasicDBList dbTagList = (BasicDBList) ((DBObject) object).get("tags");
            if(dbTagList !=null) {
                for (Object dbtag : dbTagList) {
                    tagList = new ArrayList<>();
                    Tag tag = new Tag();
                    tag.setTag((String) getField((DBObject) dbtag, "tag"));
                    tag.setDescription((String) getField((DBObject) dbtag, "description"));
                    tagList.add(tag);
                }
            }
            poll.setTags(tagList);
            pollList.add(poll);
        }
        return pollList;
    }

    private BasicDBObject createPoll(Poll poll) {
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.put("user-id", poll.getUserProfile().getEmail());
        dbObject.put("question", poll.getQuestion());
        dbObject.put("showIdentity", poll.isShowIdentity());
        dbObject.put("options", createOptions(poll.getOptions()));
        dbObject.put("tags", createTags(poll.getTags()));
        return dbObject;


    }

    private List<BasicDBObject> createTags(List<Tag> tags) {
        List<BasicDBObject> tagList = null;
        if (tags != null) {
            tagList = new ArrayList<>();
            for (Tag tag : tags) {
                BasicDBObject dbTags = new BasicDBObject();
                dbTags.put("tag", tag.getTag());
                dbTags.put("description", tag.getDescription());
                tagList.add(dbTags);
            }
        }
        return tagList;
    }

    private List<BasicDBObject> createOptions(List<Option> options) {

        List<BasicDBObject> optionList = null;
        if (options != null) {
            optionList = new ArrayList<>();
            for (Option option : options) {
                BasicDBObject dbOption = new BasicDBObject();
                dbOption.put("optionText", option.getOptionText());
                dbOption.put("optionId", option.getOptionId());
                dbOption.put("votes", createVotes(option.getVotes()));
                optionList.add(dbOption);
            }
        }
        return optionList;
    }

    private List<BasicDBObject> createVotes(List<Vote> votes) {
        List<BasicDBObject> voteList = null;
        if (null != votes) {
            voteList = new ArrayList<>();
            for (Vote vote : votes) {
                BasicDBObject dbVote = new BasicDBObject();
                dbVote.put("userId", vote.getUserProfile().getEmail());
                dbVote.put("comment", vote.getComment());
                voteList.add(dbVote);
            }
        }
        return voteList;
    }


}
