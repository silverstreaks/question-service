package com.company.question.facade;

import com.company.question.db.UserProfileRequest;
import com.company.question.model.UpdateRequest;
import com.company.question.model.UserProfile;
import com.company.question.server.Server;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import javax.swing.text.EditorKit;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.company.question.util.Constants.USER_PRFL_COLL;

/**
 * Created by Chandan on 19-02-2017 13:07.
 */
public class UserProfileFacade extends Server {

    public boolean createUserProfile(UserProfile userProfile) throws Exception{

        if (userProfile != null) {
            dataClient.setCollection(USER_PRFL_COLL);
            return dataClient.saveData(getUserProfileAttributes(userProfile));
        }
        return false;
    }

    private BasicDBObject getUserProfileAttributes(UserProfile userProfile) {
        BasicDBObject profile = null;
        profile = new BasicDBObject("_id", userProfile.getEmail()).
                append("country-code", userProfile.getCountryCode()).
                append("phone", userProfile.getPhoneNumber()).
                append("first-name", userProfile.getFirstName()).
                append("last-name", userProfile.getLastName()).
                append("active", true).
                append("reported-abuse", "0").
                append("time-stamp", new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()));
        return profile;
    }

    public boolean activateUser(UserProfile userProfile) throws Exception {
        UpdateRequest updateRequest = null;
        if (userProfile != null) {
            BasicDBObject userRecord = new BasicDBObject();
            userRecord.put("active", true);
            BasicDBObject searchQuery = new BasicDBObject().append("_id", userProfile.getEmail());
            updateRequest = new UpdateRequest(searchQuery, userRecord);
            dataClient.setCollection(USER_PRFL_COLL);
            return dataClient.update(updateRequest);
        }
        return false;
    }

    public boolean deactivateUser(UserProfile userProfile) throws Exception{
        UpdateRequest updateRequest = null;
        if (userProfile != null) {
            BasicDBObject userRecord = new BasicDBObject();
            userRecord.put("active", false);
            BasicDBObject searchQuery = new BasicDBObject().append("_id", userProfile.getEmail());
            updateRequest = new UpdateRequest(searchQuery, userRecord);
            dataClient.setCollection(USER_PRFL_COLL);
            return dataClient.update(updateRequest);
        }
        return false;
    }

    public boolean editUserProfile(UserProfile userProfile) throws Exception{
        UpdateRequest updateRequest = null;
        if (userProfile != null) {
            BasicDBObject userRecord = new BasicDBObject();
            userRecord.put("first-name", userProfile.getFirstName());
            userRecord.put("last-name", userProfile.getLastName());
            userRecord.put("country-code", userProfile.getCountryCode());
            userRecord.put("phone", userProfile.getPhoneNumber());
            BasicDBObject searchQuery = new BasicDBObject().append("_id", userProfile.getEmail());
            updateRequest = new UpdateRequest(searchQuery, userRecord);
            dataClient.setCollection(USER_PRFL_COLL);
            return dataClient.update(updateRequest);
        }
        return false;
    }

    public boolean removeUser(UserProfile user) {
        return false;
    }

    public UserProfile getUserProfile(UserProfile user) throws Exception{
        dataClient.setCollection(USER_PRFL_COLL);
        Iterator<DBObject> dbObjectIterator = dataClient.readData(createProfileSearchQuery(user)).iterator();
        while(dbObjectIterator.hasNext()){
            BasicDBObject object = (BasicDBObject)dbObjectIterator.next();
            user.setEmail((String)object.get("_id"));
            user.setFirstName((String) object.get("first-name"));
            user.setLastName((String) object.get("last-name"));
            user.setCountryCode((String)object.get("country-code"));
            user.setPhoneNumber((String)object.get("phone"));
            user.setActive(String.valueOf(object.get("active")));
            user.setReportedAbuse((String)object.get("reported-abuse"));

        }
        return user;
    }

    private BasicDBObject createProfileSearchQuery(UserProfile user) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("_id",user.getEmail());
        return searchQuery;
    }
}
