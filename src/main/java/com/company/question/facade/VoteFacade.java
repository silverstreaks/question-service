package com.company.question.facade;

import static com.company.question.util.Constants.POLL_COLL;
import static org.apache.log4j.MDC.put;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.company.question.db.UserProfileRequest;
import com.company.question.model.*;
import com.mongodb.BasicDBList;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.company.question.server.Server;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Created by Chandan on 25-02-2017 15:03.
 */
public class VoteFacade extends Server {
    private final static Logger log = Logger.getLogger(VoteFacade.class);

    public boolean postVote(Poll poll) throws Exception {
        if (null != poll) {
            dataClient.setCollection(POLL_COLL);
            return dataClient.update(createVoteRequest(poll));
        }
        return false;
    }

    private UpdateRequest createVoteRequest(Poll poll) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("_id", new ObjectId(poll.getId()));
        searchQuery.put("options.optionId", poll.getOptions().get(0).getOptionId());

        BasicDBObject updateParameter = new BasicDBObject();
        updateParameter.put("$push", new BasicDBObject("options.$.votes", createVote(poll.getOptions().get(0).getVotes().get(0))));

        return new UpdateRequest(searchQuery, updateParameter);
    }

    private BasicDBObject createVote(Vote vote) {
        BasicDBObject dbVote = null;
        if (null != vote) {
            dbVote = new BasicDBObject();
            dbVote.put("userId", vote.getUserProfile().getEmail());
            dbVote.put("comment", vote.getComment());
        }
        return dbVote;

    }

    public Poll getMyVote(Poll poll) throws Exception{
        return retrieveVotes(poll,createSearchQuery(poll),createSearchProjection("options.votes.$"));
    }

    public Poll getVotes(Poll poll) throws Exception{
        return retrieveVotes(poll,createSearchQuery(poll,true),createSearchProjection("options"));
    }

    private Poll retrieveVotes(Poll poll,BasicDBObject searchQuery,BasicDBObject searchProjection) throws Exception {
        if (poll != null) {
            //UserProfileRequest facade = new UserProfileFacade();
            //UserProfile profile = facade.getUserProfile(poll.getUserProfile());

            dataClient.setCollection(POLL_COLL);
            Iterator<DBObject> responseObject = dataClient.readData(searchQuery, searchProjection).iterator();
            List<Option> optionList = new ArrayList<>();
            Option userOption = null;
            while (responseObject.hasNext()) {
                DBObject dbResponse = responseObject.next();
                for (Object dbOption : (BasicDBList) dbResponse.get("options")) {

                    userOption = new Option();
                    userOption.setOptionId((String) getField((DBObject) dbOption, "optionId"));
                    userOption.setOptionText((String) getField((DBObject) dbOption, "optionText"));

                    List<Vote> voteList = new ArrayList<Vote>();
                    for (Object dbVote : (BasicDBList) ((DBObject)dbOption).get("votes")) {
                        Vote userVote = new Vote();
                        userVote.setComment((String) getField((DBObject) dbVote, "comment"));
                        UserProfile profile = new UserProfile();
                        userVote.setUserProfile(new UserProfile((String) getField((DBObject) dbVote, "userId")));
                        voteList.add(userVote);
                    }

                    userOption.setVotes(voteList);
                    optionList.add(userOption);
                }
                poll.setOptions(optionList);
            }
        }
        return poll;
    }

    private BasicDBObject createSearchProjection(String key) {
        BasicDBObject projection = new BasicDBObject();
        projection.put(key, "1");
        return projection;
    }

    private BasicDBObject createSearchQuery(Poll poll, boolean isWithoutUser){
        BasicDBObject searchParam = null;
        if(poll!=null){
            searchParam = new BasicDBObject();
            searchParam.put("_id", new ObjectId(poll.getId()));
        }
        return searchParam;
    }
    private BasicDBObject createSearchQuery(Poll poll) {
        BasicDBObject searchParam = null;
        if (poll != null) {
            searchParam = new BasicDBObject();
            searchParam.put("_id", new ObjectId(poll.getId()));
            searchParam.put("options.votes.userId", poll.getUserProfile().getEmail());
        }
        return searchParam;
    }
}
