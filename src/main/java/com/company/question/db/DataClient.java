package com.company.question.db;


import com.company.question.model.*;
import com.company.question.server.UserProfileServer;
import com.mongodb.*;
import org.apache.log4j.Logger;
import org.bson.Document;

import com.mongodb.client.FindIterable;

public class DataClient extends MongoDBClient {

    private final static Logger log = Logger.getLogger(DataClient.class);

    private static DataClient client;

    private DataClient() {
        createConnection();
    }

    public static synchronized DataClient getInstance() {
        if (null == client) {
            client = new DataClient();
        }
        return client;
    }


    public boolean saveData(BasicDBObject object) throws Exception {
        return save(getCollection(), object);
    }

    private boolean save(DBCollection collection, BasicDBObject object) throws Exception {
        boolean returnFlag = false;
        try {

            getCollection().insert(object);
            returnFlag = true;
        } catch (Exception e) {
            throw e;
        }
        return returnFlag;
    }

    public boolean update(UpdateRequest request) throws Exception {

        WriteResult result = null;
        if (request != null && getCollection() != null) {
            result = getCollection().update(request.getSearchQuery(), request.getUpdateParameter());
            if (result.getN() > 0) {
                return true;
            } else {
                log.error("Update Query: " + request.getSearchQuery().toString() + request.getUpdateParameter().toString());
                log.error("Error while saving update, " + result.toString());
                return false;
            }
        } else {
            throw new Exception("Invalid update request");
        }
    }

    public DBCursor readData(DBObject... object) throws Exception {
        if (object != null || object.length != 0) {
            if (object.length == 0)
                return getCollection().find();
            else if (object.length > 1)
                return getCollection().find(object[0], object[1]);
            else
                return getCollection().find(object[0]);

        }
        return null;
    }



    public void setCollection(String collection) {
        super.setCollection(collection);
    }

}
