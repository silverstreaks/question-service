package com.company.question.db;

import com.company.question.model.UpdateRequest;
import com.company.question.model.UserProfile;
import com.mongodb.BasicDBObject;

/**
 * Created by Chandan on 19-02-2017 12:26.
 */
public interface UserProfileRequest {
    public boolean createUserProfile(UserProfile userProfile) throws Exception;
    public boolean activateUser(UserProfile userProfile) throws Exception;
    public boolean deactivateUser(UserProfile userProfile) throws Exception;
    public boolean editUserProfile(UserProfile userProfile) throws Exception;
    public boolean removeUser(UserProfile user);
    public UserProfile getUserProfile(UserProfile user) throws Exception;
}
