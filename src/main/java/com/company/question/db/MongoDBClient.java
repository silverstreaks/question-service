package com.company.question.db;

import com.mongodb.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.company.question.util.Constants.*;

public class MongoDBClient {

  private DB database;
  private DBCollection collection;

  protected void createConnection() {

    // To connect to mongodb server
    initialiseLocal();
    //initialiseProduction();

  }

  private void initialiseLocal(){
    MongoClient mongoClient = new MongoClient(HOST_LOCAL, HOST_LOCAL_PORT);
    this.database = mongoClient.getDB(DATABASE);
  }

  private void initialiseProduction(){
    MongoCredential credential = MongoCredential.createCredential("admin","admin","vTCmGn33E9NP".toCharArray());
    MongoClient mongoClient = new MongoClient(new ServerAddress("127.10.103.2", HOST_LOCAL_PORT), Arrays.asList(credential));
    // Now connect to your databases
    this.database = mongoClient.getDB(PROD_DATABASE);

  }

  protected DBCollection getCollection() {
    return this.collection;
  }

  protected void setCollection(String collection) {
    this.collection = database.getCollection(collection);
  }
}
