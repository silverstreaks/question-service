package com.company.question.model;

import java.util.List;

/**
 * Created by Chandan on 18-02-2017.
 */
public class Option {
    private String optionText;
    private List<Vote> votes;
    private String optionId;

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
