package com.company.question.model;

/**
 * Created by Chandan on 18-02-2017.
 */
public class Tag {

    private String tag;
    private String description;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
