package com.company.question.model;

import java.util.List;

/**
 * Created by Chandan on 18-02-2017.
 */
public class Poll {
    private UserProfile userProfile;
    private String question;
    private List<Option> options;
    private List<Tag>  tags;
    private String showIdentity;
    private String id;

    public String getShowIdentity() {
        return showIdentity;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String isShowIdentity() {
        return showIdentity;
    }

    public void setShowIdentity(String showIdentity) {
        this.showIdentity = showIdentity;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
}
