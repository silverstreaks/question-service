package com.company.question.model;

/**
 * Created by Chandan on 18-02-2017.
 */
public class Vote {
    private UserProfile userProfile;
    private String comment;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
