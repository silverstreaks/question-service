package com.company.question.model;

import com.mongodb.BasicDBObject;

/**
 * Created by Chandan on 20-02-2017 21:28.
 */
public class UpdateRequest {

    private BasicDBObject searchQuery;
    private BasicDBObject updateParameter;

    public UpdateRequest(BasicDBObject searchQuery, BasicDBObject updateParameter) {
        this.searchQuery = searchQuery;
        this.updateParameter = updateParameter;
    }

    public BasicDBObject getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(BasicDBObject searchQuery) {
        this.searchQuery = searchQuery;
    }

    public BasicDBObject getUpdateParameter() {
        return updateParameter;
    }

    public void setUpdateParameter(BasicDBObject updateParameter) {
        this.updateParameter = updateParameter;
    }
}
