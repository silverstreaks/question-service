package com.company.question.model;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Chandan on 18-02-2017.
 */
public class UserProfile {
    private String firstName;
    private String lastName;
    private String email;
    private String countryCode;
    private String phoneNumber;
    private String active;
    private String reportedAbuse;
    private String timeStamp;
    private List<Tag> interestArea;

    public UserProfile(){}

    public UserProfile(String email){
        this.email = email;
    }

    public String isActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getReportedAbuse() {
        return reportedAbuse;
    }

    public void setReportedAbuse(String reportedAbuse) {
        this.reportedAbuse = reportedAbuse;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Tag> getInterestArea() {
        return interestArea;
    }

    public void setInterestArea(List<Tag> interestArea) {
        this.interestArea = interestArea;
    }
}
