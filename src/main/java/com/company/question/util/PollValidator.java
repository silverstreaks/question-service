package com.company.question.util;

import com.company.question.facade.UserProfileFacade;
import com.company.question.model.Poll;


/**
 * Created by Chandan on 18-02-2017.
 */
public class PollValidator extends Validator {

    public static final int VALIDATE_QUESTION = 2;
    public static final int VALIDATE_OPTION = 3;
    public static final int VALIDATE_VOTE = 4;
    public static final int VALIDATE_ID = 1;
    public static final int VALIDATE_USER_PROFILE = 5;
    public static final int VALIDATE_VOTE_PROFILE = 6;


    public static boolean validate(Poll poll, int... validators) {
        boolean validation = false;
        if (validators != null) {
            validation = true;
            for (int validator : validators) {
                if (validation) {
                    if (validator == VALIDATE_QUESTION)
                        validation &= isValidQuestion(poll);
                    else if (validator == VALIDATE_OPTION)
                        validation = validation & hasOption(poll);
                    else if (validator == VALIDATE_VOTE)
                        validation &= hasVote(poll);
                    else if (validator == VALIDATE_USER_PROFILE)
                        validation &= hasUserProfile(poll);
                    else if(validator == VALIDATE_VOTE_PROFILE)
                        validation &= hasVoteProfile(poll);
                    else
                        validation &= isValidId(poll);
                } else {
                    return validation;
                }
            }
        }
        return validation;
    }

    public static boolean isUserActive(Poll poll) throws Exception{
          return "true".equalsIgnoreCase(new UserProfileFacade().getUserProfile(poll.getUserProfile()).isActive());
    }

    public static boolean isValidQuestion(Poll poll) {
        return hasUserProfile(poll) && hasString(poll.getQuestion());
    }

    public static boolean isValidVoteReadRequest(Poll poll) {
        return hasString(poll.getId()) && hasOption(poll) && hasVote(poll) && hasVoteProfile(poll);
    }

    public static boolean isValidVotePostRequest(Poll poll) {
        return hasString(poll.getId()) && hasString(poll.getQuestion()) && hasOption(poll) && hasVote(poll) && hasVoteProfile(poll) && hasTags(poll);
    }

    private static boolean isValidId(Poll poll) {
        return hasString(poll.getId());
    }

    private static boolean hasTags(Poll poll) {
        return poll.getTags() != null;
    }


    protected static boolean hasUserProfile(Poll poll){
        return poll.getUserProfile() != null;
    }

    private static boolean hasOption(Poll poll) {
        return poll.getOptions() != null && poll.getOptions().get(0) != null;
    }

    private static boolean hasVote(Poll poll) {
        return poll.getOptions().get(0).getVotes() != null && poll.getOptions().get(0).getVotes().get(0) != null;
    }

    private static boolean hasVoteProfile(Poll poll) {
         return poll.getOptions().get(0).getVotes().get(0).getUserProfile() != null;
    }
    public static boolean validateString(String obj){
        return hasString(obj);
    }
}
