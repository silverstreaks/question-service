package com.company.question.util;

import com.company.question.model.Poll;
import com.company.question.server.VoteServer;
import org.apache.log4j.Logger;

import static org.apache.commons.lang3.StringUtils.*;


/**
 * Created by Chandan on 18-02-2017.
 */
abstract public class Validator {

    protected final static Logger log = Logger.getLogger(Validator.class);
    //protected static final int VALIDATE_USER_PROFILE = 1;

    //protected static final int VALIDATE_STRING = 5;
    //protected static final int VALIDATE_TAGS = 6;

    protected static boolean hasString(String str){
        return isNotEmpty(str);
    }

   //abstract public boolean validate(Object object,int... validators);
}
