package com.company.question.util;

import com.company.question.model.UserProfile;

/**
 * Created by Chandan on 18-02-2017 22:46.
 */
public class UserProfileValidator extends Validator{

    public static boolean isValidUser(UserProfile user){
        if(user != null){
            return hasString(user.getFirstName()) | hasString(user.getLastName()) | hasString(user.getEmail()) ;
        }
        return false;
    }
}
