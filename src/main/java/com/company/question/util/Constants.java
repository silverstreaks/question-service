package com.company.question.util;

/**
 * Created by Chandan on 18-02-2017.
 */
public class Constants {

    // Database
    public static String DATABASE = "poll";
    public static String PROD_DATABASE = "opinion";

    // Collections
    public static String USER_PRFL_COLL = "user-profile";
    public static String POLL_COLL = "poll";
    public static String TAG_COLL = "tag";

    //Connection
    public static String HOST_LOCAL = "localhost";
    public static int HOST_LOCAL_PORT = 27017 ;

    public static String HOST_PROD = "127.10.103.2"; //TBD
    public static int HOST_PROD_PORT = 27017 ;    //TBD

    //Status Codes
    public static int OK_CODE = 200;
    public static int CREATED_CODE = 201;
    public static int BAD_REQUEST_CODE = 400;
    public static int NOT_FOUND_CODE = 404;

    //Status message
    public static String OK_MSG = "Success";
    public static String CREATED_MSG = "Created";
    public static String BAD_REQUEST_MSG = "Bad Request";
    public static String NOT_FOUND_MSG = "Not Found";



}
