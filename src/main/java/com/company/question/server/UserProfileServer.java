package com.company.question.server;

import static com.company.question.util.Constants.*;
import static com.company.question.util.UserProfileValidator.isValidUser;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.company.question.facade.UserProfileFacade;
import com.company.question.db.UserProfileRequest;
import com.company.question.model.UserProfile;
import com.mongodb.DuplicateKeyException;
import org.apache.log4j.Logger;

/**
 * Root resource (exposed at "user-profile" path)
 */
@Path("user-profile")
public class UserProfileServer  {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    private final static Logger log = Logger.getLogger(UserProfileServer.class);
    private UserProfileFacade facade;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Welcome to User Profile Service";
    }

    @POST
    @Path("/addUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(UserProfile user) {
        if (null != user) {
            //Add User
            try {
                if (isValidUser(user)) {
                    log.debug("Add User: Validation successful");
                    facade = new UserProfileFacade();
                    if (facade.createUserProfile(user)) {
                        log.debug("Add User : User profile created successfully");
                        return Response.status(OK_CODE).entity(OK_MSG).build();
                    } else {
                        log.debug("Add User : User profile creation failed");
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Adding User failed, Some error Occurred.").build();
                    }
                }
            }catch (Exception ex){
                if(ex instanceof DuplicateKeyException){
                    log.error("User already exists");
                    return Response.status(Response.Status.BAD_REQUEST).entity("User already exists").build();
                }
                log.error("Exception",ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error while adding profile, Please try again later").build();
            }
        }
            return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

    @POST
    @Path("/activateUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response activateUser(UserProfile user) {
        if (user != null) {
            try {
                facade = new UserProfileFacade();
                if (facade.activateUser(user)) {
                    log.info("Activation successful");
                    return Response.status(OK_CODE).entity(OK_MSG).build();
                } else {
                    log.error("Activation failed for user : " + user.getEmail());
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Activation failed, some error occurred").build();
                }
            }catch (Exception ex){
                log.error("Exception",ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Activate failed").build();
            }
        }
        return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

    @POST
    @Path("/deactivateUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deactivateUser(UserProfile user) {
        if (user != null) {
            try {
                facade = new UserProfileFacade();
                if (facade.deactivateUser(user)) {
                    log.info("Deactivation successful");
                    return Response.status(OK_CODE).entity(OK_MSG).build();
                } else {
                    log.error("Deactivation failed for user : " + user.getEmail());
                    return Response.status(400).entity("Deactivation failed, some error occurred").build();
                }
            }catch (Exception ex){
                log.error(ex.getMessage());
                return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
            }
        }
        return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

    @POST
    @Path("/editUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editUserProfile(UserProfile user) {
        if (user != null) {
            try {
                facade = new UserProfileFacade();
                if (facade.editUserProfile(user)) {
                    log.info("User profile update successful");
                    return Response.status(OK_CODE).entity(OK_MSG).build();
                } else {
                    log.error("User profile update failed");
                    return Response.status(400).entity("Update failed, Some error occurred").build();
                }
            }catch (Exception ex){
                log.error(ex.getMessage());
                return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
            }
        }
        return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

    @POST
    @Path("/removeUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeUser(UserProfile user) {
        if (user != null) {
            try {
                facade = new UserProfileFacade();
                if (facade.removeUser(user)) {
                    log.info("User profile update successful");
                    return Response.status(OK_CODE).entity(OK_MSG).build();
                } else {
                    log.error("User profile update failed");
                    return Response.status(400).entity("Update failed, Some error occurred").build();
                }
            }catch (Exception ex){
                log.error(ex.getMessage());
                return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
            }
        }
        return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

    @POST
    @Path("/getUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserProfile(UserProfile user) {
        if(user!=null) {
            try {
                facade = new UserProfileFacade();
                UserProfile profile = facade.getUserProfile(user);
                if (null != profile) {
                    log.debug("User profile retrieved");
                    return Response.status(OK_CODE).entity(profile).build();
                } else {
                    log.error("Error retrieving profile");
                    return Response.status(400).entity("Error retrieving profile").build();
                }
            }catch (Exception ex){
                log.error(ex.getMessage());
                return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
            }
        }
        return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

}
