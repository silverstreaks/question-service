package com.company.question.server;

import com.company.question.facade.PollFacade;
import com.company.question.model.Poll;
import com.company.question.util.PollValidator;
import jersey.repackaged.com.google.common.collect.Lists;
import org.apache.log4j.Logger;

import static com.company.question.util.PollValidator.*;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

import static com.company.question.util.Constants.*;


@Path("poll")
public class PollServer {

    private final static Logger log = Logger.getLogger(PollServer.class);
    private PollFacade facade;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Welcome to Poll Service";
    }

    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postQuestion(Poll poll) {
        facade = new PollFacade();
        if (null != poll && validate(poll,VALIDATE_USER_PROFILE)) {
            try {
                //save question
                if (isUserActive(poll) && facade.addQuestion(poll)) {
                    log.debug("Added question successfully");
                    return Response.status(Response.Status.OK).entity(OK_MSG).build();
                } else {
                    log.error("Unable to add question, Question " + poll.getQuestion() + "; User :" + poll.getUserProfile().getEmail());
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to add question, Some error occurred").build();
                }
            } catch (Exception ex) {
                log.error("Some error ocurred, Please try again later",ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Some error ocurred, Please try again later").build();
            }
        }
        return Response.status(BAD_REQUEST_CODE).entity(BAD_REQUEST_MSG).build();
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateQuestion(Poll poll) {
        return null;
    }

    @POST
    @Path("/remove")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeQuestion(Poll poll) {
        return null;
    }

    @GET
    @Path("/getPolls")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQuestion() {
            try {
                facade = new PollFacade();
                //save question
                List<Poll> pollList =  facade.getQuestions();
                if (pollList !=null) {
                    log.debug("Retrieved questions successfully");
                    GenericEntity<List<Poll>> entity = new GenericEntity<List<Poll>>(Lists.newArrayList(pollList)) {};
                    return Response.status(Response.Status.OK).entity(entity).build();
                } else {
                    log.error("Unable to retrieve questions.");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to read question, Some error occurred").build();
                }
            } catch (Exception ex) {
                log.error("Some error ocurred, Please try again later",ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Some error ocurred, Please try again later").build();
            }
    }

    @GET
    @Path("/getPollById")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQuestionById(@HeaderParam("_id") String id) {
        try {
            facade = new PollFacade();
            //save question
            List<Poll> pollList =  facade.getQuestions();
            if (pollList !=null) {
                log.debug("Retrieved questions successfully");
                return Response.status(Response.Status.OK).entity(pollList).build();
            } else {
                log.error("Unable to retrieve questions.");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to read questions, Some error occurred").build();
            }
        } catch (Exception ex) {
            log.error("Some error ocurred, Please try again later",ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Some error ocurred, Please try again later").build();
        }
    }

    @POST
    @Path("/getPollByUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQuestionByUser(Poll poll) {
        if(poll!=null ){


        }
        return null;
    }

}
