package com.company.question.server;

import org.apache.commons.lang3.StringUtils;

import com.company.question.db.DataClient;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Created by Chandan on 18-02-2017.
 */
public class Server {

    protected DataClient dataClient;

    public Server() {
        this.dataClient = DataClient.getInstance();
    }
    
    protected Object getField(DBObject o, String fieldName) {

    	int index = fieldName.indexOf('.');
   	 	if(index == -1){
   	 		index= 0;
   	 	}
   	 	
   	 	String fieldPart = fieldName.substring(0,index);
        String remainder = clean(fieldName.substring(index));

        if(StringUtils.isEmpty(fieldPart))
        	return o.get(remainder);
        
        Object val = null;
        val = o.get(fieldPart);
        if(val instanceof BasicDBList){
        	return getField(((BasicDBObject)((BasicDBList)val).get(0)),remainder);
        }else {
        	return getField(((DBObject)val), remainder);
        	
        }
    }
    private String clean(String string) {
    	String cleanedString = string;
    	if(string.indexOf('.')==0){
    		cleanedString = string.substring(1);
    	}
		return cleanedString;
	}

}
