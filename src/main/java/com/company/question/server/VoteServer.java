package com.company.question.server;

import com.company.question.facade.VoteFacade;
import com.company.question.model.Poll;
import com.company.question.model.UserProfile;
import com.company.question.util.PollValidator;
import com.company.question.util.Validator;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.company.question.util.Constants.BAD_REQUEST_MSG;
import static com.company.question.util.Constants.OK_MSG;
import static com.company.question.util.PollValidator.*;

/**
 * Created by Chandan on 18-02-2017.
 */

@Path("vote")
public class VoteServer {

    private final static Logger log = Logger.getLogger(VoteServer.class);
    private VoteFacade facade;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Welcome to vote Service";
    }

    @POST
    @Path("/postVote")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postVote(Poll poll) {
        if (poll != null && validate(poll,VALIDATE_ID,VALIDATE_OPTION,VALIDATE_VOTE)) {
            try {
                facade = new VoteFacade();
                if (facade.postVote(poll)) {
                    log.debug("vote posted successfully by " + poll.getOptions().get(0).getVotes().get(0).getUserProfile().getEmail());
                    return Response.status(Response.Status.OK).entity(OK_MSG).build();
                } else {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to post vote, Some error occurred").build();
                }
            } catch (Exception ex) {
                log.error("Error Occurred", ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to post your vote").build();
            }
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(BAD_REQUEST_MSG).build();
    }

    @GET
    @Path("/getMyVote")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMyVote(@HeaderParam("_id") String id, @HeaderParam("userId")String userid) {
        Poll poll = createVoteRequest(id,userid);
        if (poll != null && validate(poll, VALIDATE_ID,VALIDATE_USER_PROFILE)) {
            try {
                facade = new VoteFacade();
                poll = facade.getMyVote(poll);
                log.debug("Vote read for question " + poll.getId());
                return Response.status(Response.Status.OK).entity(poll).build();
            } catch (Exception ex) {
                log.error("Unable to get your votes, Please try after some time", ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to get your votes, Please try after some time").build();
            }

        }
        return Response.status(Response.Status.BAD_REQUEST).entity(BAD_REQUEST_MSG).build();
    }

    @GET
    @Path("/getVotes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVotes(@HeaderParam("_id") String id) {
        if (validateString(id)) {
            try {
                facade = new VoteFacade();
                Poll poll = new Poll();
                poll.setId(id);
                poll = facade.getVotes(poll);
                log.debug("Vote read for question " + poll.getId());
                return Response.status(Response.Status.OK).entity(poll).build();
            } catch (Exception ex) {
                log.error("Unable to get your votes, Please try after some time", ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unable to get your votes, Please try after some time").build();
            }

        }
        return Response.status(Response.Status.BAD_REQUEST).entity(BAD_REQUEST_MSG).build();
    }

    private Poll createVoteRequest(String id, String userid) {
        Poll poll = null;
        if(validateString(id) && validateString(userid)){
            poll = new Poll();
            poll.setId(id);
            poll.setUserProfile(new UserProfile(userid));
        }
        return poll;
    }

    @POST
    @Path("/removeVote")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeVote() {
        return null;
    }

    @POST
    @Path("/updateVote")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateVote() {
        return null;
    }

}
